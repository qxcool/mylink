<?php

/**
 * 首页控制器
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

namespace app\index\controller;
use think\Controller;

class Index extends Controller
{
    public function index()
    {

        $this->redirect('/static/home/index.html');

        //return $this->fetch();
    }

    // pc客户端
    public function chat()
    {
        // 跳转到移动端
        if(request()->isMobile()){
            $param = http_build_query([
                'mytoken' => input('param.mytoken'),
                'group' => input('param.group')
            ]);
            $this->redirect('/index/index/mobile?' . $param);
        }

        $this->assign([
            'socket' => config('socket'),
            'mytoken' => input('param.mytoken'),
            'group' => input('param.group')
        ]);

        return $this->fetch();
    }

    // 移动客户端
    public function mobile()
    {
        $this->assign([
            'socket' => config('socket'),
            'mytoken' => input('param.mytoken'),
            'group' => input('param.group')
        ]);

        return $this->fetch();
    }
}

