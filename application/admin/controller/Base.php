<?php

/**
 * 基类控制器
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

namespace app\admin\controller;
use think\Controller;

class Base extends Controller
{
    public function _initialize()
    {
        if(empty(cookie('user_name'))){
            $this->redirect(url('login/index'));
        }

        $this->assign([
            'version' => config('version')
        ]);
    }
}
