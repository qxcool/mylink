<?php

/**
 * 基类控制器
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

namespace app\service\controller;
use think\Controller;

class Base extends Controller
{

    public function _initialize()
    {
        //检测用户是否登录
        if(empty(cookie('l_user_name'))){
            $this->redirect(url('login/index'));
        }

        //注入模板变量
        $this->assign([
            'version' => config('version'),
            'socket' => config('socket')
        ]);

    }

}