<?php

/**
 * 公共函数文件
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

//
/**
 * 删除目录以及其下的文件
 * @param $directory
 * @return bool
 */
function removeDir($directory)
{
    if (false == is_dir($directory)) {
        return false;
    }

    $handle = opendir($directory);
    while (false !== ($file = readdir($handle))) {
        if ('.' != $file && '..' != $file) {
            is_dir("$directory/$file") ? removeDir("$directory/$file") : @unlink("$directory/$file");
        }
    }

    if (readdir($handle) == false) {
        closedir($handle);
        rmdir($directory);
    }

    return true;
}