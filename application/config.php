<?php

/**
 * 通用配置文件
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

return [
	// +----------------------------------------------------------------------
	// | 应用设置
	// +----------------------------------------------------------------------

	// 当前系统版本
	'version' => 'v1.2.0',

	// 加密盐
	'salt' => 'mbku.net',

	// socket server
//	'socket' => '47.95.196.67:8282',
    'socket' => '47.95.196.67:9508',

	// 管理员登录时间
	'save_time' => 86400,

	// 应用命名空间
	'app_namespace' => 'app',
	// 应用调试模式
	'app_debug' => true,
	// 应用Trace
	'app_trace' => false
];
