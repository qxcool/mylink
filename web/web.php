<?php

/**
 * swoole 服务端
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

include_once "function.php";
include_once "controller/Api.php";
include_once "model/Db.php";
include_once "model/Cache.php";
include_once "model/ApiModel.php";

//实例化swoole的websocket服务
$serv = new swoole_websocket_server('0.0.0.0',9508);

//监听websocket连接
$serv->on('open',function($ws,$request){

    Cache::setCliUser($request->fd,['id'=>$request->fd, 'ip'=>$request->server['remote_addr']]);

    //控制台 调试输出 可以删除
    dump("新用户 {$request->fd} 加入");

});

//监听websocket发送上来的信息
$serv->on('message',function($ws,$request){

    //将用户发送上来的数据格式化成数组（客户端上传的json数据）
    $data = json_decode($request->data,true);
    if($type = $data['type']){
        Api::$type($data,$ws,$request);
    }

});

//监听websocket断开连接
$serv->on('close',function($ws,$ws_uid){

    Api::close($ws,$ws_uid);

    //控制台 调试输出 可以删除
    dump("{$ws_uid} 关闭连接");

});

/**
 * 监听worker启动
 */
$serv->on('WorkerStart', function ($serv, $worker_id){

    // 只有当worker_id为0时才添加定时器,避免重复添加
    if ($worker_id == 0) {

        swoole_timer_tick(1000*60, function () {
            ApiModel::saveNowData(1);
        });

        swoole_timer_tick(1000*60*40, function () {
            ApiModel::saveNowData(2);
        });


    }
});


//初始化
Api::start();

//启动服务器
$serv->start();