/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : mylink

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-06-06 21:36:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ws_admins`
-- ----------------------------
DROP TABLE IF EXISTS `ws_admins`;
CREATE TABLE `ws_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '密码',
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '邮箱地址',
  `last_login_ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `add_time` int(10) unsigned DEFAULT '0' COMMENT '添加时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ws_admins
-- ----------------------------
INSERT INTO `ws_admins` VALUES ('1', 'admin', '54517d0c07fee0200b68e7b065b330f6', null, '118.205.186.142', '1527606176', '0', '1');

-- ----------------------------
-- Table structure for `ws_chat_log`
-- ----------------------------
DROP TABLE IF EXISTS `ws_chat_log`;
CREATE TABLE `ws_chat_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` varchar(55) NOT NULL COMMENT '网页用户随机编号(仅为记录参考记录)',
  `from_name` varchar(255) NOT NULL COMMENT '发送者名称',
  `from_avatar` varchar(255) NOT NULL COMMENT '发送者头像',
  `to_id` varchar(55) NOT NULL COMMENT '接收方',
  `to_name` varchar(255) NOT NULL COMMENT '接受者名称',
  `content` text NOT NULL COMMENT '发送的内容',
  `time_line` int(10) unsigned NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`),
  KEY `fromid` (`from_id`(4)) USING BTREE,
  KEY `toid` (`to_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=339 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_chat_log
-- ----------------------------

-- ----------------------------
-- Table structure for `ws_groups`
-- ----------------------------
DROP TABLE IF EXISTS `ws_groups`;
CREATE TABLE `ws_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分组id',
  `name` varchar(255) NOT NULL COMMENT '分组名称',
  `status` tinyint(1) NOT NULL COMMENT '分组状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_groups
-- ----------------------------
INSERT INTO `ws_groups` VALUES ('1', '售前组', '1');
INSERT INTO `ws_groups` VALUES ('2', '售后组', '1');

-- ----------------------------
-- Table structure for `ws_kf_config`
-- ----------------------------
DROP TABLE IF EXISTS `ws_kf_config`;
CREATE TABLE `ws_kf_config` (
  `id` int(11) NOT NULL,
  `max_service` int(11) NOT NULL COMMENT '每个客服最大服务的客户数',
  `change_status` tinyint(1) NOT NULL COMMENT '是否启用转接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_kf_config
-- ----------------------------
INSERT INTO `ws_kf_config` VALUES ('1', '10', '1');

-- ----------------------------
-- Table structure for `ws_now_data`
-- ----------------------------
DROP TABLE IF EXISTS `ws_now_data`;
CREATE TABLE `ws_now_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_talking` int(5) NOT NULL DEFAULT '0' COMMENT '正在咨询的人数',
  `in_queue` int(5) NOT NULL DEFAULT '0' COMMENT '排队等待的人数',
  `online_kf` int(5) NOT NULL COMMENT '在线客服数',
  `success_in` int(5) NOT NULL COMMENT '成功接入用户',
  `total_in` int(5) NOT NULL COMMENT '今日累积接入的用户',
  `now_date` varchar(10) NOT NULL COMMENT '当前日期',
  PRIMARY KEY (`id`),
  KEY `now_date` (`now_date`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_now_data
-- ----------------------------

-- ----------------------------
-- Table structure for `ws_reply`
-- ----------------------------
DROP TABLE IF EXISTS `ws_reply`;
CREATE TABLE `ws_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL COMMENT '自动回复的内容',
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '是否自动回复',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_reply
-- ----------------------------
INSERT INTO `ws_reply` VALUES ('1', '欢迎使用MyLink客服系统', '2');

-- ----------------------------
-- Table structure for `ws_service_data`
-- ----------------------------
DROP TABLE IF EXISTS `ws_service_data`;
CREATE TABLE `ws_service_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_talking` int(5) NOT NULL DEFAULT '0' COMMENT '正在咨询的人数',
  `in_queue` int(5) NOT NULL DEFAULT '0' COMMENT '排队等待的人数',
  `online_kf` int(5) NOT NULL COMMENT '在线客服数',
  `success_in` int(5) NOT NULL COMMENT '成功接入用户',
  `total_in` int(5) NOT NULL COMMENT '今日累积接入的用户',
  `add_date` varchar(10) NOT NULL COMMENT '写入的日期',
  `add_hour` varchar(2) NOT NULL COMMENT '写入的小时数',
  `add_minute` varchar(2) NOT NULL COMMENT '写入的分钟数',
  PRIMARY KEY (`id`),
  KEY `add_date,add_hour` (`add_date`,`add_hour`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=975 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_service_data
-- ----------------------------

-- ----------------------------
-- Table structure for `ws_service_log`
-- ----------------------------
DROP TABLE IF EXISTS `ws_service_log`;
CREATE TABLE `ws_service_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(55) NOT NULL COMMENT '会员的id',
  `client_id` varchar(20) NOT NULL COMMENT '会员的客户端标识',
  `user_name` varchar(255) DEFAULT NULL COMMENT '会员名称',
  `user_avatar` varchar(155) NOT NULL COMMENT '会员头像',
  `data` text COMMENT '返回数据 json格式',
  `user_ip` varchar(15) NOT NULL COMMENT '会员的ip',
  `kf_id` varchar(55) NOT NULL COMMENT '服务的客服id',
  `start_time` int(10) NOT NULL COMMENT '开始服务时间',
  `end_time` int(10) DEFAULT '0' COMMENT '结束服务时间',
  `group_id` int(11) NOT NULL COMMENT '服务的客服的分组id',
  PRIMARY KEY (`id`),
  KEY `user_id,client_id` (`user_id`,`client_id`) USING BTREE,
  KEY `kf_id,start_time,end_time` (`kf_id`,`start_time`,`end_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_service_log
-- ----------------------------

-- ----------------------------
-- Table structure for `ws_users`
-- ----------------------------
DROP TABLE IF EXISTS `ws_users`;
CREATE TABLE `ws_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客服id',
  `user_name` varchar(255) NOT NULL COMMENT '客服名称',
  `user_pwd` varchar(32) NOT NULL COMMENT '客服登录密码',
  `user_avatar` varchar(255) NOT NULL COMMENT '客服头像',
  `status` tinyint(1) NOT NULL COMMENT '用户状态',
  `online` tinyint(1) NOT NULL DEFAULT '2' COMMENT '是否在线',
  `group_id` int(11) DEFAULT '0' COMMENT '所属分组id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_users
-- ----------------------------
INSERT INTO `ws_users` VALUES ('6', 'ztjzxf', '4c41fc59b350a1e912296f50bc799a6d', '/uploads/20180418/899067fd565c9013bb170e36c7dcaabe.png', '1', '1', '1');
INSERT INTO `ws_users` VALUES ('7', 'ztlong2', '4c41fc59b350a1e912296f50bc799a6d', '/uploads/20180511/26d3c761253665acfb8853a085b06fda.jpg', '1', '1', '2');

-- ----------------------------
-- Table structure for `ws_words`
-- ----------------------------
DROP TABLE IF EXISTS `ws_words`;
CREATE TABLE `ws_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL COMMENT '常用语内容',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `status` tinyint(1) NOT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ws_words
-- ----------------------------
INSERT INTO `ws_words` VALUES ('1', '欢迎来到MyLink客服系统', '2017-10-25 12:51:09', '1');
